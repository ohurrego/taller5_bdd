var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When(/^I open the login|register screen$/, () => {
    browser.scroll('button=Ingresar');
    browser.waitForVisible('button=Ingresar', 5000, false);
    browser.scroll('button=Ingresar');
    browser.click('button=Ingresar');
  });

  When(/^I fill with (.*) and (.*)$/ , (email, password) => {
    var cajaLogIn = browser.element('.cajaLogIn');

   var mailInput = cajaLogIn.element('input[name="correo"]');
   mailInput.click();
   mailInput.keys(email);

   var passwordInput = cajaLogIn.element('input[name="password"]');
   passwordInput.click();
   passwordInput.keys(password)
});

When(/^I fill register with (.*), (.*) and (.*)$/ , (email, password, terms) => {
  var cajaSignUp = browser.element('.cajaSignUp');

  var nameInput = cajaSignUp.element('input[name="nombre"]');
  nameInput.waitForVisible(5000, false);
  nameInput.click();
  nameInput.keys('oscar');

  var apellidoInput = cajaSignUp.element('input[name="apellido"]')
  apellidoInput.click();
  apellidoInput.keys('urrego');

  var mailInput = cajaSignUp.element('input[name="correo"]');
  mailInput.click();
  mailInput.keys(email);

  var pregradoList = cajaSignUp.element('select[name="idPrograma"]')
  pregradoList.selectByVisibleText('Derecho');

 var passwordInput = cajaSignUp.element('input[name="password"]');
 passwordInput.click();
 passwordInput.keys(password)

var terminosCheck = cajaSignUp.element('input[name="acepta"]');
if(terms === "true"){
  terminosCheck.click();
}

var registerButton = cajaSignUp.element('button=Registrarse');
registerButton.click();

});

Then('I expect to see {string}', error => {
   browser.waitForVisible('.aviso.alert.alert-danger', 10000);
   var alertText = browser.element('.aviso.alert.alert-danger').getText();
   expect(alertText).to.include(error);
});

Then('I expect message {string}', message => {
  browser.waitForVisible('h2*=Ocurrió', 10000);
  var errorText = browser.getText('h2*=Ocurrió')
  expect(errorText).to.include(message);
});

  When(/^I try to login|register$/, () => {
    var cajaLogIn = browser.element('.cajaLogIn');
    cajaLogIn.element('button=Ingresar').click()
  });

  Then('I expect the Ingresar button does not show', () => {
    browser.waitForVisible('button[id="cuenta"]', 5000);
    var cuentaButton = browser.element('button[id="cuenta"]');
    expect(cuentaButton);
 });

});