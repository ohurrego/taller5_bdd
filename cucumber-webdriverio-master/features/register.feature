Feature: Register into losestudiantes
    As an user I want to register myself within losestudiantes website in order to authenticate

Scenario Outline: Register failed with wrong inputs

  Given I go to losestudiantes home screen
    When I open the register screen
    And I fill register with <email>, <password> and <terms>
    And I try to register
    Then I expect to see <error>

    Examples:
      | email             | password | terms | error                                           |
      | taller5@gmail.com |          | true  | Ingresa una contraseña                          |
      |                   | Avril112 | true  | Ingresa tu correo                               |
      | taller5           | Avril112 | true  | Ingresa un correo valido                        |
      | taller5@gmail     | Avril112 | true  | Ingresa un correo valido                        |
      | taller5@gmail.com | Avril112 | false | Debes aceptar los términos y condiciones        |
      | taller5@gmail.com | Avril    | true  | La contraseña debe ser al menos de 8 caracteres |

  Scenario Outline: Register failed with email already registered

  Given I go to losestudiantes home screen
    When I open the register screen
    And I fill register with <email>, <password> and <terms>
    And I try to register
    Then I expect message <error>

    Examples:
      | email                     | password | terms | error                                |
      | oh.urrego@uniandes.edu.co | Avril112 | true  | Ocurrió un error activando tu cuenta |