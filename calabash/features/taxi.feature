Feature: Opening the Taxi calculator to know the price of my travel

  Scenario: As a user I want to be able to open the taxi calculator and calculate my travel with option puerta a puerta
    Given I press "Paraderos"
    When I swipe left
    And I press "Calcular tarifa taxi"
    And I wait for 1 second
    And I enter "120" into input field number 1
    Then I should see "$10,500"

  Scenario: As a user I want to be able to open the taxi calculator and calculate my travel with option Noct/fest
    Given I press "Menú de navegación cerrado"
    When I swipe left
    And I press "Calcular tarifa taxi"
    And I wait for 1 second
    And I press "cardNight"
    And I enter "200" into input field number 1
    Then I should see "$19,100"

Scenario: As a user I want to be able to open the taxi calculator and calculate my travel with option Noct/fest
    Given I press "Menú de navegación cerrado"
    When I swipe left
    And I press "Calcular tarifa taxi"
    And I wait for 1 second
    And I press "cardNight"
    And I press "cardTerminal"
    And I press "cardAirport"
    And I enter "300" into input field number 1
    Then I should see "$31,900"
  
