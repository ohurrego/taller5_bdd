Feature: Opening the rutre wish

  Scenario: As a user I want to be able to open the rute COMPLEMENTARIO
    Given I press "Menú de navegación cerrado"              
    And I press "btnTuLlaveTime"
    And I press "Rutas de buses"
    And I wait for 2 seconds
    And I press "COMPLEMENTARIO"
    And I wait for 1 second
    And I press "Tibabita"
    And I wait for 2 seconds
    Then I should see "Recorrido: Tibabita"

  Scenario: As a user I want to be able to open the rute URBANO
    Given I press "Rutas de buses"
    And I wait for 2 seconds
    And I press "URBANO"
    And I wait for 1 second
    And I press "El Dorado » Palmitas"
    And I wait for 2 seconds
    Then I should see "Recorrido: El Dorado » Palmitas"

  Scenario: As a user I want to be able to open the rute URBANO
    Given I press "Rutas de buses"              
    And I wait for 2 seconds
    And I press "ESPECIAL"
    And I wait for 1 second
    And I press "Quiba"
    And I wait for 2 seconds
    Then I should see "Recorrido: Quiba"